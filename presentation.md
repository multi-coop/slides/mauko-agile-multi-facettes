---
title: Référentiel Open Data 🇫🇷
author: multi.coop
date: 12 avril 2023
title-slide-attributes:
    data-background-image: "static/logo.svg"
    data-background-size: "auto 10%"
    data-background-position: "95% 95%"
---

# Contexte

Comment démocratiser l'open-data dans les collectivités ?

# L'existant

![Observatoire Open Data France des collectivités ayant déjà publié sur data.gouv.fr](images/observatoire.png)

# Problématique

Nous n'avions pas de moyen simple d'identifier les collectivités qui ne publient pas encore leurs données.

* Aider ces collectivités
* Dresser un état des lieux
* Cadrer des ressources
* Identifier des blocages
* ...

# Référentiel

Initialement, le projet était un livrable intermédiaire pour Open Data France.
Son potentiel d'utilisation nous pousse à élargir son usage.

Nous avons donc mis en place une liste des organisations soumises à l'obligation d'ouverture des données en France.

---

![Exploration](images/explo_obligation.png)

# Méthodologie

Le CRPA définit que les administrations de l'Etat, les collectivités territoriales ainsi que les organisations chargées d’une mission de service public ont l'obligation d'ouvrir leurs données (open data), **sauf celles qui emploient moins de 50 équivalents temps plein (ETP) ainsi que les collectivités territoriales de moins de 3 500 habitants**.

Pour identifier les entités, nous avons récupéré et filtré la base SIRENE en fonction d'un sous-ensemble de catégorie juridiques.

Afin de savoir si une entité était obligée de publier selon les règles du CRPA, nous avons enrichi cette base SIRENE d'un champ population.


# Limites

* Recensement obscure pour certaines entités "d'intérêt public"
* Sourcing du champ population parfois compliqué

Ces limites soulignent l'importance de créer ce référentiel collectivement.
